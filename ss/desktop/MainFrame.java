/*
    Copyright (C) 2011, 2015, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import groovy.lang.GroovyClassLoader;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.*;
import java.io.*;
import java.net.URI;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;

import ss.MainWindow;
import ss.PropertiesWorker;
import ss.ScriptContainer;
import ss.TextSource;
import ss.WebCommunicator;

/**
 * <p>
 * Start class, main frame
 * </p>
 * <p>
 * Refactoring goal : only UI here
 * </p>
 * 
 * @author Doti
 */
public class MainFrame extends JFrame implements TextSource, MainWindow {

	private static final long serialVersionUID = 1L;

	private static final int DEFAULT_HEIGHT = 700;
	private static final int DEFAULT_WIDTH = 900;
	private static final int NB_MAX_BUTTONS = 5;
	private static final int DELAY_SHOW_WAIT_ICON_S = 60;
	private static final int SHOWBUTTON_WAIT_SECURITY_MARGIN_MS = 10;

	private boolean discreetMode;
	private boolean stealthMode;
	private ScriptContainer sc;
	private PropertiesWorker propertiesWorker;
	// Animated gifs : path -> ImageIcon -> Label
	private ImageIcon imageIcon = null;
	private String text = "";
	private JLabel label;
	private Gauge gauge;
	private PrettyButton button;
	private JTextField textField;
	private PrettyButton[] buttons;
	private JComponent lastChoice;
	private JComboBox<String> comboBox;
	private float speed = 1;
	private Thread thread = null;
	private JMenuItem mReset;
	private JMenuItem mAutoCopy;
	private JMenu mSpeed;
	private JFileChooser fc;
	private EditorFrame editor;
	private ImageIcon backgroundImage;
	private JLabel infoLabel;
	private long buttonStartTimestamp;
	private float buttonResultSeconds;
	private String lastTextForClipboard;

	public MainFrame(String[] args) {
		super();
		ArgumentParser ap = new ArgumentParser(args); 
		discreetMode = ap.containsKey("discreet") || ap.containsKey("t");
		stealthMode = ap.containsKey("stealth") || ap.containsKey("s") || discreetMode;
		if(ap.containsKey("data"))
			propertiesWorker = new PropertiesWorker(this, ap.get("data"));
		else if(ap.containsKey("d"))
			propertiesWorker = new PropertiesWorker(this, ap.get("d"));
		else
			propertiesWorker = new PropertiesWorker(this, DEFAULT_PROPERTIES_FILENAME);
		propertiesWorker.updateSystemProperties();
		sc = new ScriptContainer();
		createMenu();
		createFrame();		
		startIntroScript(args);
	}

	private void startIntroScript(String[] args) {
		String startScript = propertiesWorker.loadString("intro.start_script");
		ArgumentParser ap = new ArgumentParser(args);
		if (ap.containsKey("default"))
			startScript = ap.get("default");
		if (ap.containsKey("script"))
			startScript = ap.get("script");		
		if (startScript == null)
			startScript = "intro";
		launchSpecialScript(startScript);
	}

	@Override
	public void dispose() {
		stopScriptThreads();
		if (editor != null)
			editor.dispose();
		super.dispose();
	}

	public File getDataFolder() {
		return new File(".");
	}

	public File getTmpFolder() {
		return new File(".");
	}

	public String readScript(String fileName) {
		sc.currentScriptFileName = fileName;
		try {
			sc.readFromFile();
			if (!sc.forgetScriptAtEnd) {
				String name = new File(getDataFolder(),
						sc.currentScriptFileName).getName();
				name = name.substring(0, name.lastIndexOf("."));
				setTitle(name);
			}
			if (editor != null)
				editor.setText(sc.currentScriptText);
			updateMenu();
			return sc.currentScriptText;
		} catch (Exception e) {
			showMessage("Could not read : " + e, true);
			sc.currentScriptFileName = null;
			sc.currentScriptText = "";
			updateMenu();
			return "";
		}
	}

	public void writeScript(String fileName, boolean alsoUpdateInterface) {
		if (!fileName.toLowerCase().endsWith(".groovy"))
			fileName += ".groovy";
		try {
			Writer f = new OutputStreamWriter(new FileOutputStream(fileName),
					Charset.forName("UTF-8"));
			f.write(sc.currentScriptText);
			f.close();
			if (alsoUpdateInterface) {
				sc.currentScriptFileName = fileName;
				// new Publisher(editor, this).createAndroidFiles(fileName);
				String name = new File(sc.currentScriptFileName).getName();
				name = name.substring(0, name.lastIndexOf("."));
				setTitle(name);
				updateMenu();
			}
		} catch (Exception e) {
			showMessage("Could not write : " + e, true);
		}
	}
	
	@Override
	public File getFileFromChooser(boolean groovyFilter, File baseFolder, String dialogTitle, Object parent) {
		if(fc==null) {
			try {
				fc = new JFileChooser();
			} catch (Exception ex) {
				Main.setLookAndFeel(false);
				fc = new JFileChooser();
			}
		}
		if(baseFolder!=null)
			fc.setCurrentDirectory(new File(SCRIPT_FOLDER));
		if(dialogTitle!=null)
			fc.setDialogTitle(dialogTitle);
		if(groovyFilter) {
			fc.addChoosableFileFilter(new FileFilter() {
				public String getDescription() {
					return getString("groovy_ext_descr");
				}
	
				public boolean accept(File f) {
					return f.isDirectory()
							|| f.getName().toLowerCase().endsWith(".groovy");
				}
			});
		}
		int res;
		if(parent!=null)
			res = fc.showOpenDialog((Component)parent);
		else
			res = fc.showOpenDialog(this);
		Main.setLookAndFeel(true);
		if (res == JFileChooser.APPROVE_OPTION) {
			sc.forgetScriptAtEnd = false;
			return fc.getSelectedFile();
		}
		return null;		
	}

	protected void showOnlineHelp() {
		Desktop desktop = Desktop.getDesktop();
		try {
			desktop.browse(new URI(WEBSITE_ONLINEHELP_URL));
		} catch (Exception e) {
			showMessage(
					getString("error",
							WEBSITE_ONLINEHELP_URL + " " + e.toString()), true);
		}
	}

	public void showAbout() {
		String lastAuthor = sc.savedAuthor;
		if (lastAuthor == null)
			lastAuthor = "/";
		String localModified = "?";
		String yearsString = "2011-";
		Date datelocalModified = getLocalModifiedDate();
		if (datelocalModified != null) {
			DateFormat df = DateFormat.getDateInstance();
			localModified = df.format(datelocalModified);
			yearsString += new SimpleDateFormat("yyyy")
					.format(datelocalModified);
		}
		String onlineModified = "?";
		WebCommunicator webCommunicator = new WebCommunicator();
		Date dateOnlineModified = webCommunicator.getOnlineModifiedDate(
			WebCommunicator.OnlineModifiedFileType.ZIP);
		if (dateOnlineModified != null) {
			DateFormat df = DateFormat.getDateInstance();
			onlineModified = df.format(dateOnlineModified);
		}
		GraphicsDevice device = GraphicsEnvironment
				.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		if (device.getFullScreenWindow() == MainFrame.this)
			device.setFullScreenWindow(null);		
		JOptionPane.showMessageDialog(
				null,
				getString("about", yearsString, localModified, onlineModified,
						getString("translator"), lastAuthor),
				getString("title"), JOptionPane.OK_OPTION
						| JOptionPane.INFORMATION_MESSAGE);
	}

	public void changeColor(int colori) {
		Color color = new Color(colori);
		((JPanel) getContentPane()).setBackground(color);
		if (color.getRed() + color.getGreen() + color.getBlue() < 128 * 3)
			label.setForeground(Color.WHITE);
		else
			label.setForeground(Color.BLACK);
		button.setColor(color);
		for (PrettyButton pb : buttons)
			pb.setColor(color);
	}

	@Override
	public void setTitle(String title) {
		title = title.trim();
		if (!title.contains(getString("title"))) {
			if (title.equals(""))
				title = getString("title");
			else
				title = getString("title") + " - " + title;
		}
		if(Main.DEBUG_MODE)
			title += " - DEBUG MODE !";
		super.setTitle(title);
		if (editor != null)
			editor.setTitle(title);
	}

	public void updateTextAndImage() {

		if (imageIcon != null) {
			ImageIcon usedII = imageIcon;
			double maxTooBigRatio = Math.max(1, imageIcon.getIconWidth()
					/ (.6 * getWidth()));
			maxTooBigRatio = Math.max(maxTooBigRatio, imageIcon.getIconHeight()
					/ (.8 * getHeight()));
			if (maxTooBigRatio > 1) {
				usedII = new ImageIcon(imageIcon.getImage().getScaledInstance(
						(int) (imageIcon.getIconWidth() / maxTooBigRatio),
						(int) (imageIcon.getIconHeight() / maxTooBigRatio),
						Image.SCALE_SMOOTH));
				if (usedII.getImageLoadStatus() != MediaTracker.COMPLETE) {
					// probably animated GIF
					usedII = new ImageIcon(
							imageIcon
									.getImage()
									.getScaledInstance(
											(int) (imageIcon.getIconWidth() / maxTooBigRatio),
											(int) (imageIcon.getIconHeight() / maxTooBigRatio),
											Image.SCALE_FAST));
				}
			}
			if (!discreetMode)
				label.setIcon(usedII);
		} else {
			label.setIcon(null);
		}

		lastTextForClipboard = text;
		if (text.equals("")) {
			label.setText("");
		} else {
			int iconWidth = 0;
			if (label.getIcon() != null)
				iconWidth = label.getIcon().getIconWidth();
			int textWidth = getWidth() - 20 - label.getIconTextGap()
					- iconWidth;
			int textHeight = getHeight() - 60;
			float baseSize = (float) (1.2f * Math.sqrt(textHeight * textWidth
					/ (text.length() + text.split("\n").length * 45)));
			float size = Math.round(10 * Math.max(4, Math.min(19, baseSize))) / 10;
			if (discreetMode)
				size /= 2;
			if (size != label.getFont().getSize())
				label.setFont(label.getFont().deriveFont(size));
			label.setText("<html>" + text.trim().replace("\n", "<br />")
					+ "</html>");
			if (mAutoCopy.isSelected())
				copyTextToClipboard();
		}
	}

	public void setWaiting(boolean waiting) {
		if (editor != null) {
			if (waiting)
				editor.setCursor(new Cursor(Cursor.WAIT_CURSOR));
			else
				editor.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		}
		if (waiting)
			setCursor(new Cursor(Cursor.WAIT_CURSOR));
		else
			setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}

	public void showMessage(String text, boolean error) {
		int messageType = JOptionPane.INFORMATION_MESSAGE;
		if (error)
			messageType = JOptionPane.ERROR_MESSAGE;
		GraphicsDevice device = GraphicsEnvironment
				.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		if (device.getFullScreenWindow() == MainFrame.this)
			device.setFullScreenWindow(null);
		JOptionPane.showMessageDialog(this, text, getString("title"),
				messageType);
	}

	public String getInputString(String string, String defaultString) {
		GraphicsDevice device = GraphicsEnvironment
				.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		if (device.getFullScreenWindow() == MainFrame.this)
			device.setFullScreenWindow(null);
		return JOptionPane.showInputDialog(this, getString("import.url"),
				defaultString);
	}

	public PropertiesWorker getPropertiesWorker() {
		return propertiesWorker;
	}

	public ScriptContainer getScriptContainer() {
		return sc;
	}

	public void showFromScript(String s) {
		if (s == null)
			text = "";
		else
			text = s;
		updateTextAndImage();
	}

	public float showPopupFromScript(String s) {
		buttonStartTimestamp = System.currentTimeMillis();
		showMessage(s, false);
		buttonResultSeconds = (System.currentTimeMillis() - buttonStartTimestamp) / 1000.0f;
		return speed * buttonResultSeconds;
	}

	public double showButtonFromScript(String s, double duration) {
		if (s == null)
			s = "";
		buttonStartTimestamp = System.currentTimeMillis();
		button.setText("<html>" + s.trim().replace("\n", "<br />") + "</html>");
		button.setVisible(true);

		button.requestFocusInWindow();
		try {
			synchronized (button) {
				button.wait((long) (duration * 1000 / speed)
						+ SHOWBUTTON_WAIT_SECURITY_MARGIN_MS);
				button.setVisible(false);
				buttonResultSeconds = speed
						* (System.currentTimeMillis() - buttonStartTimestamp)
						/ 1000.0f;
			}
		} catch (InterruptedException e) {
		}

		return Math.min(duration, buttonResultSeconds);
	}

	public String getStringFromScript(String text, String defaultValue) {
		if (text != null) {
			showFromScript(text);
		}
		textField.setText(defaultValue);
		textField.setVisible(true);
		buttons[1].setText((String) UIManager.get("OptionPane.okButtonText"));
		buttons[1].setVisible(true);
		textField.requestFocusInWindow();
		try {
			synchronized (buttons[1]) {
				buttons[1].wait();
				textField.setVisible(false);
				buttons[1].setVisible(false);
			}
		} catch (InterruptedException e) {
		}
		return textField.getText();
	}

	public boolean getBooleanFromScript(String text, String yesMessage,
			String noMessage) {
		if (text != null) {
			showFromScript(text);
		}
		buttons[0].setText(yesMessage);
		buttons[0].setVisible(true);
		buttons[1].setText(noMessage);
		buttons[1].setVisible(true);
		buttons[0].requestFocusInWindow();
		boolean res = false;
		try {
			synchronized (buttons[1]) {
				buttons[1].wait();
				res = buttons[0] == lastChoice;
				buttons[0].setVisible(false);
				buttons[1].setVisible(false);
			}
		} catch (InterruptedException e) {
		}
		return res;
	}

	@Override
	public int getSelectedValueFromScript(String text, List<String> values) {
		if (text != null) {
			showFromScript(text);
		}
		if (values.size() > NB_MAX_BUTTONS)
			return getSelectedValueFromScriptByCombo(text, values);
		int totalCars = 0;
		for (String v : values)
			totalCars += v.length() + 5; // left and right of buttons : 2.5 cars
		if (totalCars > getWidth() * .1)
			return getSelectedValueFromScriptByCombo(text, values);
		else
			return getSelectedValueFromScriptByButtons(text, values);
	}

	private int getSelectedValueFromScriptByCombo(String text,
			List<String> values) {
		comboBox.removeAllItems();
		comboBox.addItem("<html><i>?</em></html>");
		for (String value : values)
			comboBox.addItem(value);
		comboBox.setVisible(true);
		comboBox.setMaximumRowCount(10);
		comboBox.requestFocusInWindow();
		try {
			synchronized (comboBox) {
				comboBox.wait();
				comboBox.setVisible(false);
			}
		} catch (InterruptedException e) {
		}
		return comboBox.getSelectedIndex() - 1;
	}

	private int getSelectedValueFromScriptByButtons(String text,
			List<String> values) {
		for (int i = 0; i < values.size(); i++) {
			buttons[i].setText(values.get(i));
			buttons[i].setVisible(true);
		}
		buttons[0].requestFocusInWindow();
		int res = 0;
		try {
			synchronized (buttons[1]) {
				buttons[1].wait();
				for (int i = 0; i < buttons.length; i++) {
					if (lastChoice == buttons[i])
						res = i;
					buttons[i].setVisible(false);
				}
			}
		} catch (InterruptedException e) {
		}
		return res;
	}

	public void waitFromScript(double duration) {
		try {
			// 10 waits (and not 1) : in case of speed change, and for icon
			for (int i = 0; i < 10; i++) {
				Thread.sleep((int) (duration * 100 / speed));
				if (duration * i / 10 > DELAY_SHOW_WAIT_ICON_S
						&& !infoLabel.isEnabled())
					updateInfoLabel("waiting", true);
			}
			updateInfoLabel("waiting", false);
		} catch (InterruptedException e) {
		}
	}

	public void waitWithGaugeFromScript(double duration) {
		try {
			gauge.setVisible(true);
			int msTot = (int) (duration * 1000 / speed);
			for (int i = 0; i < msTot; i += 40) {
				gauge.setPrc(100 * (double) i / msTot);
				repaint();
				Thread.sleep(40);
				// in case of speed change
				msTot = (int) (duration * 1000 / speed);
			}
			gauge.setVisible(false);
		} catch (InterruptedException e) {
		}
	}

	public void setImageFromScript(String imagePath) {
		if (imagePath == null)
			this.imageIcon = null;
		else
			this.imageIcon = new ImageIcon(imagePath);
		updateTextAndImage();
	}

	public void setImageFromScript(byte[] bytes) {
		if (bytes == null)
			this.imageIcon = null;
		else
			this.imageIcon = new ImageIcon(bytes);
		updateTextAndImage();
	}

	private boolean launchSpecialScript(String name) {
		sc.forgetScriptAtEnd = true;
		return launchScript(name);
	}

	private boolean launchScript(String name) {
		String nameWithoutExt = name;
		if (nameWithoutExt.toLowerCase().endsWith(".groovy"))
			nameWithoutExt = nameWithoutExt.substring(0, nameWithoutExt
					.toLowerCase().lastIndexOf(".groovy"));
		String[] wantedFileNames = {
				new File(getDataFolder(), MainWindow.SCRIPT_FOLDER
						+ nameWithoutExt + "_"
						+ Locale.getDefault().getLanguage() + "_"
						+ Locale.getDefault().getCountry() + ".groovy")
						.getAbsolutePath(),
				new File(getDataFolder(), MainWindow.SCRIPT_FOLDER
						+ nameWithoutExt + "_"
						+ Locale.getDefault().getLanguage() + ".groovy")
						.getAbsolutePath(),
				new File(getDataFolder(), MainWindow.SCRIPT_FOLDER
						+ nameWithoutExt + ".groovy").getAbsolutePath(),
				new File(nameWithoutExt + ".groovy").getAbsolutePath()};
		String currentScript = "";
		for (String wantedFileName : wantedFileNames)
			if (new File(wantedFileName).exists()) {
				currentScript = readScript(wantedFileName);
				break;
			}
		if (currentScript.equals(""))
			return false;
		run();
		return true;
	}

	public void run() {

		stopScriptThreads();
		thread = new Thread() {
			@SuppressWarnings("unchecked")
			public void run() {
				// always wait a little bit
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				hideScriptInteractionComponents();
				GroovyClassLoader gcl = new GroovyClassLoader();
				String nextScript = null;
				try {
					String fullScript = sc.getScriptClassText(false);
					updateMenu();
					if (fullScript == null) {
						showMessage(getString("error", "No script"), true);
						gcl.close();
						return;
					}
					Class<Script> c = (Class<Script>) gcl
							.parseClass(fullScript);
					sc.currentScript = (Script) c.getConstructor(
							MainWindow.class, TextSource.class).newInstance(
							MainFrame.this, MainFrame.this);
					nextScript = (String) sc.currentScript.run();
					sc.currentScript.stopSoundThreads();
					gcl.close();
				} catch (Exception e) {
					e.printStackTrace();
					showMessage(getString("error", e.getMessage()), true);
				}
				// end
				boolean ended;
				if (nextScript != null)
					ended = !launchScript(nextScript);
				else
					ended = true;
				if (ended) {
					changeColor(DEFAULT_COLOR);
					if (sc.forgetScriptAtEnd) {
						sc.currentScriptFileName = null;
						sc.currentScriptText = "";
						sc.savedAuthor = null;
						sc.forgetScriptAtEnd = false;
					}
					setTitle("");
				}
				updateMenu();
			}
		};
		thread.start();
	}

	@SuppressWarnings("deprecation")
	public void stopScriptThreads() {
		if (sc.currentScript != null)
			sc.currentScript.stopSoundThreads();
		try {
			Thread.sleep(Script.WAIT_SOUND_THREAD_MS);
		} catch (InterruptedException e) {
		}
		if (thread != null && thread.isAlive()
				&& thread != Thread.currentThread())
			thread.stop();
	}

	public String getString(String s, Object... args) {
		try {
			ResourceBundle bundle = ResourceBundle.getBundle("texts");
			if(stealthMode && bundle.containsKey("stealth."+s))
				return String.format(bundle.getString("stealth."+s), args);
			else
				return String.format(bundle.getString(s), args);
		} catch (Exception e) {
			return String.format(s, args);
		}
	}

	public void hideScriptInteractionComponents() {
		gauge.setVisible(false);
		infoLabel.setEnabled(false);
		button.setVisible(false);
		for (JButton bt : buttons)
			bt.setVisible(false);
		textField.setVisible(false);
		comboBox.setVisible(false);
	}

	private void createMenu() {
		JMenuBar jMenuBar = new JMenuBar();
		setJMenuBar(jMenuBar);

		JMenu mFile = new JMenu(getString("menu.file"));
		mFile.setMnemonic('F');
		jMenuBar.add(mFile);

		JMenuItem mOpen = new JMenuItem(getString("menu.open"), new ImageIcon(
				getClass().getResource("images/open.png")));
		mOpen.setMnemonic('O');
		mOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
				ActionEvent.CTRL_MASK));
		mOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GraphicsDevice device = GraphicsEnvironment
						.getLocalGraphicsEnvironment().getDefaultScreenDevice();
				if (device.getFullScreenWindow() == MainFrame.this)
					device.setFullScreenWindow(null);
				File dstFile = getFileFromChooser(true, new File(SCRIPT_FOLDER), null, null);				
				if (dstFile != null) {
					sc.forgetScriptAtEnd = false;
					readScript(dstFile.getAbsolutePath());
					run();
				}
			}
		});
		mFile.add(mOpen);

		JMenuItem mImport = new JMenuItem(getString("menu.import"),
				new ImageIcon(getClass().getResource("images/import.png")));
		mImport.setMnemonic('I');
		mImport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sc.askForAndImportScript(MainFrame.this, getClipboardUrl());
			}
		});
		mFile.add(mImport);

		mFile.add(new JSeparator());

		JMenuItem mExit = new JMenuItem(getString("menu.exit"), new ImageIcon(
				getClass().getResource("images/exit.png")));
		mExit.setMnemonic('E');
		mExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,
				ActionEvent.CTRL_MASK));
		mExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		mFile.add(mExit);

		JMenu mScript = new JMenu(getString("menu.script"));
		mScript.setMnemonic('S');
		jMenuBar.add(mScript);

		mReset = new JMenuItem(getString("menu.reset"), new ImageIcon(
				getClass().getResource("images/reset.png")));
		mReset.setMnemonic('R');
		mReset.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,
				ActionEvent.CTRL_MASK));
		mReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (sc.currentScriptFileName == null)
					startIntroScript(null);
				else
					run();
			}
		});
		mScript.add(mReset);

		JMenuItem mEdit = new JMenuItem(getString("menu.edit"), new ImageIcon(
				getClass().getResource("images/edit.png")));
		mEdit.setMnemonic('E');
		mEdit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,
				ActionEvent.CTRL_MASK));
		mEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				launchEditor();
				editor.setVisible(true);
			}
		});
		mScript.add(mEdit);

		JMenu mOptions = new JMenu(getString("menu.options"));
		mOptions.setMnemonic('O');
		jMenuBar.add(mOptions);

		JMenuItem mToys = new JMenuItem(getString("menu.toys"), new ImageIcon(
				getClass().getResource("images/options.png")));
		mToys.setMnemonic('T');
		mToys.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (thread != null && thread.isAlive()) {
					GraphicsDevice device = GraphicsEnvironment
							.getLocalGraphicsEnvironment().getDefaultScreenDevice();
					if (device.getFullScreenWindow() == MainFrame.this)
						device.setFullScreenWindow(null);					
					if (JOptionPane.showConfirmDialog(MainFrame.this,
							getString("warning.interrupt"), getString("title"),
							JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION)
						return;
				}
				launchSpecialScript("toys");
			}
		});
		mOptions.add(mToys);

		JMenuItem mWClothes = new JMenuItem(getString("menu.womensclothes"),
				new ImageIcon(getClass().getResource("images/options.png")));
		mWClothes.setMnemonic('W');
		mWClothes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (thread != null && thread.isAlive()) {
					GraphicsDevice device = GraphicsEnvironment
							.getLocalGraphicsEnvironment().getDefaultScreenDevice();
					if (device.getFullScreenWindow() == MainFrame.this)
						device.setFullScreenWindow(null);					
					if (JOptionPane.showConfirmDialog(MainFrame.this,
							getString("warning.interrupt"), getString("title"),
							JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION)
						return;
				}
				launchSpecialScript("womensclothes");
			}
		});
		mOptions.add(mWClothes);

		JMenuItem mMClothes = new JMenuItem(getString("menu.mensclothes"),
				new ImageIcon(getClass().getResource("images/options.png")));
		mMClothes.setMnemonic('M');
		mMClothes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (thread != null && thread.isAlive()) {
					GraphicsDevice device = GraphicsEnvironment
							.getLocalGraphicsEnvironment().getDefaultScreenDevice();
					if (device.getFullScreenWindow() == MainFrame.this)
						device.setFullScreenWindow(null);					
					if (JOptionPane.showConfirmDialog(MainFrame.this,
							getString("warning.interrupt"), getString("title"),
							JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION)
						return;
				}
				launchSpecialScript("mensclothes");
			}
		});
		mOptions.add(mMClothes);

		mOptions.add(new JSeparator());

		mSpeed = new JMenu(getString("menu.speed"));
		mSpeed.setIcon(new ImageIcon(getClass()
				.getResource("images/normal.png")));
		mSpeed.setMnemonic('S');
		mOptions.add(mSpeed);

		JMenuItem mS1 = new JCheckBoxMenuItem(getString("menu.speed.slow"),
				new ImageIcon(getClass().getResource("images/slow.png")));
		mS1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1,
				ActionEvent.CTRL_MASK));
		mS1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeSpeed(.5f);
			}

		});
		mSpeed.add(mS1);

		JMenuItem mS2 = new JCheckBoxMenuItem(getString("menu.speed.normal"),
				new ImageIcon(getClass().getResource("images/normal.png")));
		mS2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2,
				ActionEvent.CTRL_MASK));
		mS2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeSpeed(1);
			}
		});
		mSpeed.add(mS2);

		JMenuItem mS3 = new JCheckBoxMenuItem(getString("menu.speed.fast"),
				new ImageIcon(getClass().getResource("images/fast.png")));
		mS3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3,
				ActionEvent.CTRL_MASK));
		mS3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeSpeed(2);
			}
		});
		mSpeed.add(mS3);

		JMenuItem mS4 = new JCheckBoxMenuItem(getString("menu.speed.faster"),
				new ImageIcon(getClass().getResource("images/faster.png")));
		mS4.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4,
				ActionEvent.CTRL_MASK));
		mS4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeSpeed(4);
			}
		});
		mSpeed.add(mS4);

		changeSpeed(1);

		mAutoCopy = new JCheckBoxMenuItem(getString("menu.autocopy"),
				new ImageIcon(getClass().getResource("images/copy.png")));
		mAutoCopy.setMnemonic('C');
		mOptions.add(mAutoCopy);

		JMenu mHelp = new JMenu(getString("menu.help"));
		mHelp.setMnemonic('H');
		jMenuBar.add(mHelp);

		JMenuItem mOnlineHelp = new JMenuItem(getString("menu.onlinehelp"),
				new ImageIcon(getClass().getResource("images/onlinehelp.png")));
		mOnlineHelp.setMnemonic('O');
		mOnlineHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showOnlineHelp();
			}
		});
		mHelp.add(mOnlineHelp);

		mHelp.add(new JSeparator());

		JMenuItem mAbout = new JMenuItem(getString("menu.about"),
				new ImageIcon(getClass().getResource("images/about.png")));
		mAbout.setMnemonic('A');
		mAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showAbout();
			}
		});
		mHelp.add(mAbout);

		KeyboardFocusManager.getCurrentKeyboardFocusManager()
				.addKeyEventPostProcessor(new KeyEventPostProcessor() {

					@Override
					public boolean postProcessKeyEvent(KeyEvent e) {
						if (e.getID() == KeyEvent.KEY_PRESSED
								&& MainFrame.this.isActive()) {
							if (e.getKeyCode() == KeyEvent.VK_C
									&& e.isControlDown() && isActive()) {
								copyTextToClipboard();
								return true;
							}
							if ((e.getKeyCode() == KeyEvent.VK_LEFT || e
									.getKeyCode() == KeyEvent.VK_RIGHT)
									&& buttons[0].isVisible()
									&& buttons[1].isVisible()) {
								int n = 0;
								for (PrettyButton bt : buttons)
									if (bt.isVisible())
										n++;
								int newFocus = 0;
								for (int i = 0; i < n; i++)
									if (buttons[i].isFocusOwner()) {
										if (e.getKeyCode() == KeyEvent.VK_LEFT)
											newFocus = (i + n - 1) % n;
										else
											newFocus = (i + 1) % n;
										break;
									}
								buttons[newFocus].requestFocus();
							}
							if (e.isAltDown()
									&& e.getKeyCode() == KeyEvent.VK_ENTER) {
								GraphicsDevice device = GraphicsEnvironment
										.getLocalGraphicsEnvironment()
										.getDefaultScreenDevice();
								if (device.getFullScreenWindow() == MainFrame.this) {
									device.setFullScreenWindow(null);
								} else {
									device.setFullScreenWindow(MainFrame.this);
								}
							}
						}
						return false;
					}
				});
	}

	private void createFrame() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("");
		String backgroundImageFilename = propertiesWorker
				.loadString("application.background");
		if (backgroundImageFilename == null)
			backgroundImageFilename = BACKGROUND_IMAGE_FILENAME;
		backgroundImage = new ImageIcon(backgroundImageFilename);
		Image icon;
		try {
			if(stealthMode)
				icon = ImageIO.read(getClass().getResource("images/logo_stealth.png"));
			else
				icon = ImageIO.read(getClass().getResource("images/logo.png"));
			setIconImage(icon);
		} catch (IOException e1) {
		}
		JPanel panel = new JPanel(new GridBagLayout()) {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				if (this.isOpaque()) {
					g.setColor(getBackground());
					g.fillRect(0, 0, getWidth(), getHeight());
				}
				MainFrame.this.paintPanelComponent(g);
			}

		};
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(panel);

		// grid size : (2+NB_MAX_BUTTONS) width x 2 height
		GridBagConstraints c = new GridBagConstraints();

		label = new JLabel();
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setText("");
		Font font;
		String fontProp = propertiesWorker.loadString("application.font");
		if (fontProp != null)
			font = new Font(fontProp, 0, 19);
		else
			font = label.getFont().deriveFont(0, 19);
		label.setFont(font);
		label.setIconTextGap(20);
		label.setDoubleBuffered(true);
		c.gridwidth = 2 + NB_MAX_BUTTONS;
		c.weightx = 1;
		c.weighty = 1;
		c.anchor = GridBagConstraints.LINE_START;
		c.fill = GridBagConstraints.HORIZONTAL;
		panel.add(label, c);

		gauge = new Gauge(new BorderLayout());
		gauge.setBackground(Color.WHITE);
		gauge.setColor(Color.PINK);
		gauge.setPreferredSize(new Dimension(250, 40));
		gauge.setMinimumSize(new Dimension(100, 40));
		gauge.setVisible(false);
		c.gridwidth = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0.4;
		c.weighty = 0.2;
		panel.add(gauge, c);

		infoLabel = new JLabel();
		infoLabel.setText("");
		infoLabel.setEnabled(false); // enabled, not visible => keep same space
		infoLabel.setIgnoreRepaint(true);
		try {
			Image i = ImageIO
					.read(getClass().getResource("images/infooff.png"));
			infoLabel.setDisabledIcon(new ImageIcon(i));
		} catch (IOException e1) {
		}
		c.weightx = 0.2;
		panel.add(infoLabel, c);

		JPanel uselessPanel = new JPanel();
		uselessPanel.setOpaque(false);
		uselessPanel.setPreferredSize(new Dimension(10, 55));
		uselessPanel.setMinimumSize(new Dimension(0, 55));
		c.gridx = 1;
		panel.add(uselessPanel, c);

		textField = new JTextField();
		textField.setFont(font);
		textField.setVisible(false);
		textField.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					buttons[1].doClick();
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		textField.setPreferredSize(new Dimension(400, textField
				.getPreferredSize().height));
		c.gridwidth = 1;
		c.gridx = 2;
		c.weightx = 1;
		panel.add(textField, c);

		comboBox = new JComboBox<String>();
		comboBox.setFont(font);
		comboBox.setVisible(false);

		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (comboBox) {
					if (comboBox.isPopupVisible()
							&& comboBox.getSelectedIndex() > 0)
						comboBox.notify();
				}
			}
		});

		c.gridwidth = NB_MAX_BUTTONS;
		c.gridx = 2;
		panel.add(comboBox, c);

		button = new PrettyButton("...", Color.YELLOW);
		button.setFont(font);
		button.setVisible(false);
		button.setRolloverEnabled(true);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (button) {
					button.notify();
				}
			}
		});
		c.gridwidth = NB_MAX_BUTTONS;
		c.gridx = 2;
		panel.add(button, c);

		buttons = new PrettyButton[NB_MAX_BUTTONS];
		for (int n = 0; n < NB_MAX_BUTTONS; n++) {
			buttons[n] = new PrettyButton("...", Color.BLUE);
			buttons[n].setFont(font);
			buttons[n].setVisible(false);
			buttons[n].setRolloverEnabled(true);
			buttons[n].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					synchronized (buttons[1]) {
						lastChoice = (JButton) e.getSource();
						buttons[1].notify();
					}
				}
			});
			c.gridwidth = 1;
			c.gridx = 2 + n;
			c.weightx = .6 / NB_MAX_BUTTONS;
			c.fill = GridBagConstraints.NONE;
			panel.add(buttons[n], c);
		}

		pack();
		Integer width = propertiesWorker.loadInteger("application.width");
		Integer height = propertiesWorker.loadInteger("application.height");
		Integer left = propertiesWorker.loadInteger("application.left");
		Integer top = propertiesWorker.loadInteger("application.top");

		if (width == null)
			width = DEFAULT_WIDTH;
		if (height == null)
			height = DEFAULT_HEIGHT;
		if (left == null || top == null) {
			setSize(width, height);
			setLocationRelativeTo(null);
		} else {
			setBounds(left, top, width, height);
		}

		addComponentListener(new ComponentListener() {
			public void componentShown(ComponentEvent e) {
			}

			public void componentMoved(ComponentEvent e) {
				saveSizeAndPosition();
			}

			public void componentHidden(ComponentEvent e) {
			}

			@Override
			public void componentResized(ComponentEvent e) {
				saveSizeAndPosition();
				updateTextAndImage();
			}
		});
		changeColor(DEFAULT_COLOR);
		setVisible(true);
	}

	private void saveSizeAndPosition() {
		if (getState() == Frame.NORMAL) {
			propertiesWorker.save("application.width", getWidth());
			propertiesWorker.save("application.height", getHeight());
			propertiesWorker.save("application.left", getX());
			propertiesWorker.save("application.top", getY());
		}
	}

	private void paintPanelComponent(Graphics g) {
		g.drawImage(
				backgroundImage.getImage(),
				0,
				0,
				getWidth(),
				backgroundImage.getIconHeight() * getWidth()
						/ backgroundImage.getIconWidth(), 0, 0,
				backgroundImage.getIconWidth(),
				backgroundImage.getIconHeight(), null);
	}

	private void changeSpeed(float speed) {
		this.speed = speed;
		updateMenu();
	}

	public void updateMenu() {
		((JMenuItem) mSpeed.getMenuComponent(0)).setSelected(speed == .5f);
		((JMenuItem) mSpeed.getMenuComponent(1)).setSelected(speed == 1);
		((JMenuItem) mSpeed.getMenuComponent(2)).setSelected(speed == 2);
		((JMenuItem) mSpeed.getMenuComponent(3)).setSelected(speed == 4);
		if (mReset != null && sc != null) {
			mReset.setEnabled(sc.currentScriptFileName != null
					|| sc.currentScriptText == null
					|| sc.currentScriptText.equals(""));
			if (editor != null && editor.isUnsaved())
				mReset.setText(getString("menu.reset") + " * ");
			else
				mReset.setText(getString("menu.reset"));
		}
	}

	private void launchEditor() {
		if (editor == null || !editor.isDisplayable())
			editor = new EditorFrame(MainFrame.this);
		editor.setTitle(getTitle());
	}

	private Date getLocalModifiedDate() {
		Date res = null;
		try {
			File f = new File(getClass().getClassLoader()
					.getResource("ss/Main.class").toURI());
			if (f != null && f.exists())
				res = new Date(f.lastModified());
		} catch (Exception e) {
		}
		try {
			if (res == null) {
				res = new Date(new File(getClass().getProtectionDomain()
						.getCodeSource().getLocation().toURI()).lastModified());
			}
		} catch (Exception e) {
		}
		return res;
	}

	public void setPlayingSound(boolean playingSound) {
		updateInfoLabel("speaker", playingSound);
	}

	private void updateInfoLabel(String image, boolean enabled) {
		if (enabled && !image.equals(infoLabel.getName())) {
			try {
				Image i = ImageIO.read(getClass().getResource(
						"images/" + image + ".png"));
				infoLabel.setIcon(new ImageIcon(i));
			} catch (Exception e) {
			}
			infoLabel.setName(image);
		}
		if (enabled)
			infoLabel.setEnabled(true);
		if (!enabled && image.equals(infoLabel.getName())) {
			infoLabel.setEnabled(false);
		}
	}

	private void copyTextToClipboard() {
	    try {
			Clipboard clipboard = getToolkit().getSystemClipboard();
			StringSelection data = new StringSelection(lastTextForClipboard);
			clipboard.setContents(data, data);
		} catch(Exception ex){ // see post 7548
		}
	}

	public String getClipboardUrl() {
		String res = "";
		try {
			Clipboard clipboard = getToolkit().getSystemClipboard();
			String content = (String) clipboard.getContents(this)
					.getTransferData(DataFlavor.stringFlavor);
			String[] strings = content.split("\n");
			if (strings[0].contains("://"))
				res = strings[0];
		} catch (IOException e1) {
		} catch (UnsupportedFlavorException e1) {
		} catch (Exception ex){ // see post 7548
		}
		return res;
	}
}
