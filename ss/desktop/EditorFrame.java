/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import groovy.lang.GroovyClassLoader;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.KeyEventPostProcessor;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.*;

import javax.swing.*;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.*;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;

import ss.MainWindow;
import ss.ScriptContainer;
import ss.TextSource;

/**
 * <p>
 * Script editor
 * </p>
 * <p>
 * Refactoring goal : only UI here (quite ok now)
 * </p>
 * 
 * @author Doti
 */
public class EditorFrame extends JFrame {

	private static final String INFO_ADDRESS_START_STR = "@ line";
	private static final long serialVersionUID = 1L;
	private static final int TESTS_NUMBER = 20;
	private static final int INVISIBLE_LINES_COUNT = 41;
	private static final Integer DEFAULT_WIDTH = 880;
	private static final Integer DEFAULT_HEIGHT = 680;
	private static final int AUTOSAVE_MINUTES = 10;
	private static final int MAX_TEST_DURATION_SECONDS = 5;
	private static final String TEMPLATE_FILENAME = MainWindow.SCRIPT_FOLDER + "template.groovy";
	private static final String AUTOSAVE_FILENAME = MainWindow.SCRIPT_FOLDER + "autosave.groovy";

	private MainFrame main;
	private JTextPane scriptTP;
	private JTextPane infoTP;
	private JTextField searchEdit;
	private JLabel infoLabel;
	private UndoManager undoManager;
	private int lastCaretDot;
	private SyntaxHighlighter syntaxHighlighter;
	private SortedMap<String, List<Snippet>> snippets;
	private java.util.Timer autoSaveTimer;
	private Thread testThread;
	private java.util.Timer stopTestTimer;
	private long stopTestTimerStart;
	private List<LinkPosition> linkPositions;
	// 0000000 : nothing, 0000001 : autosave, 0000010 : highlighter / other,
	// 0000100 : hl / move, 1111000 : hl / change
	private int assistLevel;

	public EditorFrame(MainFrame main) {
		super();
		this.main = main;
		Integer al = main.getPropertiesWorker().loadInteger("application.editor.assistlevel");
		if (al == null)
			this.assistLevel = 255;
		else
			this.assistLevel = al;
		syntaxHighlighter = new SyntaxHighlighter();
		createMenu();
		createFrame();
		loadSnippetsAndCreateContextMenu();
		if ((assistLevel & 2) != 0)
			syntaxHighlighter.fireChange(false);
		SwingUtilities.invokeLater(syntaxHighlighter);
		startAutoSaveTimer();
	}

	protected void finalize() {
		autoSaveTimer.cancel();
		stopTestTimer.cancel();
	}

	public boolean isUnsaved() {
		return !scriptTP.getText().equals(main.getScriptContainer().currentScriptText);
	}

	private void createMenu() {
		JMenuBar jMenuBar = new JMenuBar();
		setJMenuBar(jMenuBar);

		JMenu mFile = new JMenu(main.getString("menu.file"));
		mFile.setMnemonic('F');
		jMenuBar.add(mFile);

		JMenuItem mNew = new JMenuItem(main.getString("menu.new"),
				new ImageIcon(getClass().getResource("images/new.png")));
		mNew.setMnemonic('N');
		mNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(EditorFrame.this, main.getString("warning.interrupt"),
						main.getString("title"), JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
					createNewScript();
			}
		});
		mFile.add(mNew);

		mFile.add(new JSeparator());

		JMenuItem mOpen = new JMenuItem(main.getString("menu.open"),
				new ImageIcon(getClass().getResource("images/open.png")));
		mOpen.setMnemonic('O');
		mOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		mOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tryOpen();
			}
		});
		mFile.add(mOpen);

		JMenuItem mSave = new JMenuItem(main.getString("menu.save"),
				new ImageIcon(getClass().getResource("images/save.png")));
		mSave.setMnemonic('S');
		mSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		mSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trySave();
			}
		});
		mFile.add(mSave);

		JMenuItem mSaveAs = new JMenuItem(main.getString("menu.save_as"),
				new ImageIcon(getClass().getResource("images/saveas.png")));
		mSaveAs.setMnemonic('A');
		mSaveAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ScriptContainer sc = main.getScriptContainer();
				String oldName = sc.currentScriptFileName;
				sc.currentScriptFileName = null;
				trySave();
				if (sc.currentScriptFileName == null)
					sc.currentScriptFileName = oldName;
			}
		});
		mFile.add(mSaveAs);

		mFile.add(new JSeparator());

		JMenuItem mPictureGallery = new JMenuItem(main.getString("menu.picturegallery"),
				new ImageIcon(getClass().getResource("images/picturegallery.png")));
		mPictureGallery.setMnemonic('G');
		mPictureGallery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String res = new PictureGalleryImportator().doImport(main, EditorFrame.this);
				if (res != null)
					scriptTP.setText(scriptTP.getText() + res);
			}
		});
		mFile.add(mPictureGallery);

		JMenuItem mMilovana = new JMenuItem(main.getString("menu.milovana"),
				new ImageIcon(getClass().getResource("images/milovana.png")));
		mMilovana.setMnemonic('M');
		mMilovana.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String res = new MilovanaImportator().doImport(main, EditorFrame.this);
				if (res != null)
					scriptTP.setText(scriptTP.getText() + res);
			}
		});
		mFile.add(mMilovana);

		JMenuItem mCybermistress = new JMenuItem(main.getString("menu.cybermistress"),
				new ImageIcon(getClass().getResource("images/cybermistress.png")));
		mCybermistress.setMnemonic('C');
		mCybermistress.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String res = new CyberMistressImportator().doImport(main, EditorFrame.this);
				if (res != null)
					scriptTP.setText(scriptTP.getText() + res);
			}
		});
		mFile.add(mCybermistress);

		mFile.add(new JSeparator());

		JMenuItem mPublish = new JMenuItem(main.getString("menu.publish"),
				new ImageIcon(getClass().getResource("images/publish.png")));
		mPublish.setMnemonic('P');
		mPublish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trySave();
				new Publisher(EditorFrame.this, main).execute();
			}
		});
		mFile.add(mPublish);

		mFile.add(new JSeparator());

		JMenuItem mClose = new JMenuItem(main.getString("menu.close"),
				new ImageIcon(getClass().getResource("images/exit.png")));
		mClose.setMnemonic('Q');
		mClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		mClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tryDispose();
			}
		});
		mFile.add(mClose);

		JMenu mEdit = new JMenu(main.getString("menu.edit"));
		mEdit.setMnemonic('E');
		jMenuBar.add(mEdit);

		JMenuItem mUndo = new JMenuItem(main.getString("menu.undo") + " Ctrl+Z",
				new ImageIcon(getClass().getResource("images/undo.png")));
		mUndo.setMnemonic('U');
		/*
		 * Menu shortcuts are too slow
		 * mUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,
		 * ActionEvent.CTRL_MASK));
		 */
		mUndo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				undo();
			}
		});
		mUndo.setEnabled(false);
		mEdit.add(mUndo);

		JMenuItem mRedo = new JMenuItem(main.getString("menu.redo") + " Ctrl+Y",
				new ImageIcon(getClass().getResource("images/redo.png")));
		mRedo.setMnemonic('R');
		/*
		 * mRedo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y,
		 * ActionEvent.CTRL_MASK));
		 */
		mRedo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				redo();
			}
		});
		mRedo.setEnabled(false);
		mEdit.add(mRedo);

		mEdit.add(new JSeparator());
		JMenuItem mSearch = new JMenuItem(main.getString("menu.search"),
				new ImageIcon(getClass().getResource("images/search.png")));
		mSearch.setMnemonic('e');
		mSearch.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		mSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchNext(false);
			}
		});
		mEdit.add(mSearch);
		JMenuItem mReplace = new JMenuItem(main.getString("menu.replace"),
				new ImageIcon(getClass().getResource("images/replace.png")));
		mReplace.setMnemonic('R');
		mReplace.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_J, KeyEvent.CTRL_MASK));
		mReplace.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				replaceAll();
			}
		});
		mEdit.add(mReplace);

		mEdit.add(new JSeparator());
		JMenuItem mCut = new JMenuItem(main.getString("menu.cut"),
				new ImageIcon(getClass().getResource("images/cut.png")));
		mCut.setMnemonic('C');
		mCut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_MASK));
		mCut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cutScript(e);
			}
		});
		mEdit.add(mCut);

		JMenuItem mCopy = new JMenuItem(main.getString("menu.copy"),
				new ImageIcon(getClass().getResource("images/copy.png")));
		mCopy.setMnemonic('P');
		mCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_MASK));
		mCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				copyScript();
			}
		});
		mEdit.add(mCopy);

		JMenuItem mPaste = new JMenuItem(main.getString("menu.paste"),
				new ImageIcon(getClass().getResource("images/paste.png")));
		mPaste.setMnemonic('O');
		mPaste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_MASK));
		mPaste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pasteScript();
			}

			private void pasteScript() {
				Clipboard clipboard = getToolkit().getSystemClipboard();
				try {
					scriptTP.getDocument().remove(scriptTP.getSelectionStart(),
							scriptTP.getSelectionEnd() - scriptTP.getSelectionStart());
					scriptTP.getDocument().insertString(scriptTP.getSelectionStart(),
							(String) clipboard.getContents(this).getTransferData(DataFlavor.stringFlavor), null);
				} catch (BadLocationException e1) {
				} catch (IOException e1) {
				} catch (UnsupportedFlavorException e1) {
				}
			}
		});
		mEdit.add(mPaste);

		JMenu mHelp = new JMenu(main.getString("menu.help"));
		mHelp.setMnemonic('H');
		jMenuBar.add(mHelp);

		JMenuItem mTest = new JMenuItem(main.getString("menu.check"),
				new ImageIcon(getClass().getResource("images/check.png")));
		mTest.setMnemonic('C');
		mTest.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		mTest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				test();
			}
		});
		mHelp.add(mTest);

		JMenuItem mOnlineHelp = new JMenuItem(main.getString("menu.onlinehelp"),
				new ImageIcon(getClass().getResource("images/onlinehelp.png")));
		mOnlineHelp.setMnemonic('O');
		mOnlineHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				main.showOnlineHelp();
			}
		});
		mHelp.add(mOnlineHelp);

		mHelp.add(new JSeparator());

		JMenuItem mAbout = new JMenuItem(main.getString("menu.about"),
				new ImageIcon(getClass().getResource("images/about.png")));
		mAbout.setMnemonic('A');
		mAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				main.showAbout();
			}
		});
		mHelp.add(mAbout);
	}

	private void createFrame() {
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				tryDispose();
			}
		});
		setIconImage(main.getIconImage());
		JPanel upPane = new JPanel(new BorderLayout());
		JToolBar jToolBar = new JToolBar();

		JButton open = new JButton(main.getString("menu.open"),
				new ImageIcon(getClass().getResource("images/open.png")));
		open.setToolTipText(main.getString("menu.open.tooltip"));
		open.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tryOpen();
			}
		});
		jToolBar.add(open);

		JButton save = new JButton(main.getString("menu.save"),
				new ImageIcon(getClass().getResource("images/save.png")));
		save.setToolTipText(main.getString("menu.save.tooltip"));
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trySave();
			}
		});
		jToolBar.add(save);

		jToolBar.add(new JToolBar.Separator());

		JButton check = new JButton(main.getString("menu.check"),
				new ImageIcon(getClass().getResource("images/check.png")));
		check.setToolTipText(main.getString("menu.check.tooltip"));
		check.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				test();
			}
		});
		jToolBar.add(check);

		jToolBar.add(new JToolBar.Separator());

		infoLabel = new JLabel("0/0");
		infoLabel.setHorizontalAlignment(0);
		infoLabel.setPreferredSize(new Dimension(50, infoLabel.getPreferredSize().height));
		jToolBar.add(infoLabel);

		jToolBar.add(new JToolBar.Separator());

		JLabel searchLabel = new JLabel(
				new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images/search.png"))));
		searchLabel.setBorder(new EmptyBorder(0, 5, 0, 5));
		jToolBar.add(searchLabel);
		searchEdit = new JTextField();
		searchEdit.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				search(e);
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		jToolBar.add(searchEdit);
		upPane.add(jToolBar, BorderLayout.PAGE_START);

		scriptTP = new JTextPane();
		scriptTP.setAutoscrolls(true);
		scriptTP.setFont(Font.getFont("monospaced"));
		scriptTP.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		TabStop[] tabs = new TabStop[50];
		for (int i = 0; i < tabs.length; i++) {
			tabs[i] = new TabStop((i + 1) * 29, TabStop.ALIGN_LEFT, TabStop.LEAD_NONE);
		}
		TabSet tabset = new TabSet(tabs);
		StyleContext sco = StyleContext.getDefaultStyleContext();
		AttributeSet aset = sco.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.TabSet, tabset);
		scriptTP.setParagraphAttributes(aset, false);

		ScriptContainer sc = main.getScriptContainer();
		if (sc.currentScriptText.equals(""))
			createNewScript();
		setText(sc.currentScriptText);

		scriptTP.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent ce) {
				EditorFrame.this.caretUpdate(ce);
			}
		});
		scriptTP.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_TAB)
					if (scriptTPtabPressed((e.getModifiersEx() & KeyEvent.SHIFT_DOWN_MASK) > 0))
						e.consume();
			}
		});

		JScrollPane scriptScroll = new JScrollPane(scriptTP);
		upPane.add(scriptScroll);

		infoTP = new JTextPane();
		infoTP.setAutoscrolls(true);
		infoTP.setEditable(false);
		infoTP.setToolTipText(main.getString("editor.info.tooltip"));
		infoTP.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int pos = infoTP.viewToModel(e.getPoint());
				if(linkPositions!=null) {
					for (LinkPosition lp : linkPositions) {
						if (pos > lp.start && pos < lp.end) {
							String[] lines = scriptTP.getText().split("\n");
							int startSel = 0, endSel = 0;
							int line = lp.line;
							if (line < 0)
								line = 0;
							else if (line >= lines.length)
								line = lines.length - 1;
							for (int i = 0; i < line; i++)
								startSel += lines[i].length() + 1;
							endSel = startSel + lines[line].length();
							scriptTP.setSelectionStart(startSel);
							scriptTP.setSelectionEnd(endSel);
							scriptTP.requestFocus();
						}
					}
				}
			}
		});
		JScrollPane infoScroll = new JScrollPane(infoTP);
		JSplitPane panel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, upPane, infoScroll);
		setContentPane(panel);

		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventPostProcessor(new KeyEventPostProcessor() {

			@Override
			public boolean postProcessKeyEvent(KeyEvent e) {
				if ((e.getID() & KeyEvent.KEY_PRESSED) != 0) {
					if (e.getKeyCode() == KeyEvent.VK_F && e.isControlDown()) {
						searchEdit.requestFocus();
						searchEdit.selectAll();
						return true;
					}
					if (e.getKeyCode() == KeyEvent.VK_F3 && e.isShiftDown()) {
						searchNext(true);
						return true;
					}
					if (e.getKeyCode() == KeyEvent.VK_U && e.isControlDown()) {
						if (EditorFrame.this.isActive())
							main.setVisible(true);
						return true;
					}
					if (e.getKeyCode() == KeyEvent.VK_Z && e.isControlDown() && undoManager.canUndo()) {
						undo();
						return true;
					}
					if (e.getKeyCode() == KeyEvent.VK_Y && e.isControlDown() && undoManager.canRedo()) {
						redo();
						return true;
					}

				}
				return false;
			}
		});
		pack();
		Integer width = main.getPropertiesWorker().loadInteger("application.editor.width");
		Integer height = main.getPropertiesWorker().loadInteger("application.editor.height");
		Integer left = main.getPropertiesWorker().loadInteger("application.editor.left");
		Integer top = main.getPropertiesWorker().loadInteger("application.editor.top");
		if (width == null)
			width = DEFAULT_WIDTH;
		if (height == null)
			height = DEFAULT_HEIGHT;
		if (left == null || top == null) {
			setSize(width, height);
			setLocationRelativeTo(null);
		} else {
			setBounds(left, top, width, height);
		}
		addComponentListener(new ComponentListener() {
			public void componentShown(ComponentEvent e) {
			}

			public void componentMoved(ComponentEvent e) {
				saveSizeAndPosition();
			}

			public void componentHidden(ComponentEvent e) {
			}

			@Override
			public void componentResized(ComponentEvent e) {
				saveSizeAndPosition();
			}
		});
		setVisible(true);
		panel.setDividerLocation(.7);
	}

	protected void saveSizeAndPosition() {
		if (getState() == Frame.NORMAL) {
			main.getPropertiesWorker().save("application.editor.width", getWidth());
			main.getPropertiesWorker().save("application.editor.height", getHeight());
			main.getPropertiesWorker().save("application.editor.left", getX());
			main.getPropertiesWorker().save("application.editor.top", getY());
		}
	}

	private void loadSnippetsAndCreateContextMenu() {
		ResourceBundle bundle = ResourceBundle.getBundle("snippets");
		List<String> names = new ArrayList<String>();
		List<String> groups = new ArrayList<String>();
		List<String> sortedKeys = Collections.list(bundle.getKeys());
		Collections.sort(sortedKeys);
		for (String key : sortedKeys) {
			String[] parts = key.split("\\.");
			if (!names.contains(parts[1]))
				names.add(parts[1]);
			if (parts[2].equals("group") && !groups.contains(bundle.getString(key)))
				groups.add(bundle.getString(key));
		}
		snippets = new TreeMap<String, List<Snippet>>();
		for (String group : groups) {
			List<Snippet> l = new ArrayList<Snippet>();
			for (String key : sortedKeys) {
				String[] parts = key.split("\\.");
				if (parts[2].equals("group") && bundle.getString(key).equals(group))
					l.add(new Snippet(parts[1], bundle.getString("snippets." + parts[1] + ".name"),
							bundle.getString("snippets." + parts[1] + ".text")));
			}
			snippets.put(group, l);
		}

		scriptTP.addMouseListener(new MouseListener() {

			public void popMenu(MouseEvent e) {
				JPopupMenu jpm = new JPopupMenu();
				for (String group : snippets.keySet()) {
					JMenu jmi = new JMenu(group);
					boolean barDone = false;
					for (Snippet sn : snippets.get(group)) {
						JMenuItem jmis = new JMenuItem(sn.getName());
						jmis.addActionListener(new ActionListener() {
							private Snippet sn;

							@Override
							public void actionPerformed(ActionEvent e) {
								useSnippet(sn);
							}

							public ActionListener init(Snippet sn) {
								this.sn = sn;
								return this;
							}
						}.init(sn));
						if (sn.getKeyName().startsWith("z") && !barDone) {
							jmi.add(new JSeparator(JSeparator.HORIZONTAL));
							barDone = true;
						}
						jmi.add(jmis);
					}
					jpm.add(jmi);
				}
				jpm.show(e.getComponent(), e.getX(), e.getY());
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger())
					popMenu(e);
			}

			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger())
					popMenu(e);
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
	}

	private void createNewScript() {
		main.stopScriptThreads();
		main.hideScriptInteractionComponents();
		ScriptContainer sc = main.getScriptContainer();
		sc.forgetScriptAtEnd = false;
		main.readScript(TEMPLATE_FILENAME);
		sc.currentScriptFileName = null;
		main.updateMenu();
		restartUndoRedo();
	}

	private boolean isCanQuitPerhapsSave() {
		if (isUnsaved()) {
			int res = JOptionPane.showConfirmDialog(this, main.getString("closing_save"), main.getString("title"),
					JOptionPane.YES_NO_CANCEL_OPTION);
			if (res == JOptionPane.YES_OPTION)
				trySave();
			return res == JOptionPane.YES_OPTION || res == JOptionPane.NO_OPTION;
		} else
			return true;
	}

	private void tryDispose() {
		if (isCanQuitPerhapsSave())
			dispose();
	}

	private void tryOpen() {
		if (!isCanQuitPerhapsSave())
			return;
		File dstFile = main.getFileFromChooser(true, new File(MainFrame.SCRIPT_FOLDER), null, this);
		if (dstFile != null) {
			main.stopScriptThreads();
			main.hideScriptInteractionComponents();
			ScriptContainer sc = main.getScriptContainer();
			sc.forgetScriptAtEnd = false;
			sc.savedAuthor = null;
			try {
				setText(main.readScript(dstFile.getCanonicalPath()));
			} catch (IOException ex) {
			}
			startAutoSaveTimer();
		}
	}

	private void trySave() {
		String name = null;
		ScriptContainer sc = main.getScriptContainer();
		if (sc.currentScriptFileName == null) {			
			File dstFile = main.getFileFromChooser(true, new File(MainFrame.SCRIPT_FOLDER), null, this);			
			if (dstFile != null) {
				sc.currentScriptText = scriptTP.getText();
				try {
					name = dstFile.getCanonicalPath();
				} catch (IOException ex) {
				}
			}
		} else {
			sc.currentScriptText = scriptTP.getText();
			name = sc.currentScriptFileName;
		}
		if (name != null) {
			main.writeScript(name, true);
			name = sc.currentScriptFileName;
			DateFormat df = DateFormat.getTimeInstance();
			addInfo(df.format(new Date()) + " " + main.getString("menu.save") + " " + name);
		}
	}

	private void startAutoSaveTimer() {
		if (autoSaveTimer != null)
			autoSaveTimer.cancel();
		autoSaveTimer = new java.util.Timer();
		if ((assistLevel & 1) != 0)
			autoSaveTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					autoSave();
				}
			}, AUTOSAVE_MINUTES * 60 * 1000, AUTOSAVE_MINUTES * 60 * 1000);
	}

	private void autoSave() {
		main.getScriptContainer().currentScriptText = scriptTP.getText();
		main.writeScript(AUTOSAVE_FILENAME, false);
	}

	private void test() {
		testThread = new Thread() {
			@Override
			public void run() {
				threadedTest();
			}
		};
		// will be stopped by a test in the loop (at half time), or killed by
		// this timer
		if (stopTestTimer != null)
			stopTestTimer.cancel();
		stopTestTimer = new java.util.Timer();
		stopTestTimer.schedule(new TimerTask() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				if (testThread.isAlive()) {
					testThread.stop();
					try {
						main.getPropertiesWorker().restore();
					} catch (IOException ex) {
					}
					DateFormat df = DateFormat.getTimeInstance();
					addInfo(df.format(new Date()) + " " + main.getString("parsing.stopped"));
				}
			}
		}, MAX_TEST_DURATION_SECONDS * 1000);
		stopTestTimerStart = System.currentTimeMillis();
		testThread.start();
	}

	private void threadedTest() {
		DateFormat df = DateFormat.getTimeInstance();
		addInfo(df.format(new Date()) + " " + main.getString("parsing.start"));
		GroovyClassLoader gcl = new GroovyClassLoader();
		List<Integer> statDelays = new ArrayList<Integer>();
		ScriptContainer sc = main.getScriptContainer();
		sc.currentScriptText = scriptTP.getText();
		try {
			gcl.parseClass(sc.getScriptClassText(false));
			String fullScript = sc.getScriptClassText(true);
			main.getScriptContainer().clearUsedFiles();
			sc.addToMainUsedFiles(sc.currentScriptFileName);
			@SuppressWarnings("unchecked")
			Class<ScriptStub> c = (Class<ScriptStub>) gcl.parseClass(fullScript);
			// backup+restore because ..?..
			main.getPropertiesWorker().backup();
			main.getPropertiesWorker().restore();
			main.getPropertiesWorker().backup();
			for (int seed = 0; seed < TESTS_NUMBER && System.currentTimeMillis() < stopTestTimerStart
					+ MAX_TEST_DURATION_SECONDS * 1000 * 0.5; seed++) {
				ScriptStub sStub = c.getConstructor(MainWindow.class, TextSource.class, Long.TYPE).newInstance(main,
						main, seed);
				sStub.run();
				statDelays.add(sStub.getStatDelay());
			}
			main.getPropertiesWorker().restore();
			gcl.close();
		} catch (Exception e) {
			try {
				main.getPropertiesWorker().restore();
			} catch (IOException e1) {
			}
			String s = main.getString("parsing.error", e.toString());
			for (StackTraceElement ste : e.getStackTrace()) {
				if ((ste.getClassName().equals("ss.ScriptStub") && ste.getMethodName().equals("run"))
						|| (ste.getClassName().startsWith("ss.FullScript_") && ste.getMethodName().equals("run"))
						|| (ste.getClassName().startsWith("ss.FullScript_") && ste.getMethodName().equals("doCall"))) {
					s += "(" + INFO_ADDRESS_START_STR + " " + ste.getLineNumber() + ")";
					break;
				}
			}
			addInfo(s);
			e.printStackTrace();
		}
		Collections.sort(statDelays);
		int medianTime = 0;
		if (statDelays.size() > 0) {
			int n = 0;
			for (int i = statDelays.size() / 5; i < statDelays.size() * 4 / 5; i++) {
				medianTime += statDelays.get(i);
				n++;
			}
			medianTime = (int) Math.round(medianTime / 60.0 / n);
		}
		addInfo(df.format(new Date()) + " " + main.getString("parsing.end", medianTime));
	}

	public void addInfo(String s) {
		infoTP.setText(infoTP.getText() + "\n" + s);
		StyledDocument sd = infoTP.getStyledDocument();
		SimpleAttributeSet nullAS = new SimpleAttributeSet();
		StyleConstants.setFontSize(nullAS, 12);
		SimpleAttributeSet addressAS = new SimpleAttributeSet();
		StyleConstants.setUnderline(addressAS, true);
		StyleConstants.setForeground(addressAS, new Color(0, 0, 128));
		sd.setCharacterAttributes(0, sd.getLength(), nullAS, true);
		int i = 0;
		String text = "";
		try {
			text = sd.getText(0, sd.getLength());
		} catch (BadLocationException e1) {
		}
		linkPositions = new ArrayList<EditorFrame.LinkPosition>();
		while (i > -1) {
			i = text.indexOf(INFO_ADDRESS_START_STR, i + 1);
			if (i > -1) {
				int end = i + 1;
				while (end < text.length() && (text.charAt(end) < '0' || text.charAt(end) > '9'))
					end++;
				while (end < text.length() && !(text.charAt(end) < '0' || text.charAt(end) > '9'))
					end++;
				if (end < text.length()) {
					sd.setCharacterAttributes(i, end - i, addressAS, false);
				}
				int line = 0;
				try {
					String sLineNumber = text.substring(text.lastIndexOf(" ", end) + 1, end);
					line = Integer.parseInt(sLineNumber) - INVISIBLE_LINES_COUNT - 1;
					if (line >= 0)
						linkPositions.add(new LinkPosition(i, end, line));
				} catch (NumberFormatException e) {
				}

				i = end;
			}
		}
		infoTP.setCaretPosition(infoTP.getDocument().getLength());

	}

	/**
	 * Change not from the user, not undoable
	 */
	public void setText(String text) {
		if (undoManager != null)
			scriptTP.getDocument().removeUndoableEditListener(undoManager);
		scriptTP.setText(text);
		restartUndoRedo();
	}

	/**
	 * @return Full content string of URL (not suitable for binary content)
	 */
	public String getUrlContent(String urlStr) throws MalformedURLException, IOException {
		String line, res = "";
		HttpURLConnection urlConnection = getHttpURLConnection(urlStr);
		BufferedReader dis = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		while ((line = dis.readLine()) != null) {
			res += line + "\n";
		}
		dis.close();
		return res;
	}

	public HttpURLConnection getHttpURLConnection(String urlStr) throws IOException {
		boolean redirect = false;
		int nbRedirect = 0;
		HttpURLConnection urlConnection = null;
		do {
			URL url = new URL(urlStr);
			urlConnection = (HttpURLConnection) url.openConnection();
			//some usual headers, a little bit randomness to avoid counter-counter-counter-haxors 
			urlConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.3");
			urlConnection.setRequestProperty("Accept-Encoding", "deflate");
			urlConnection.setRequestProperty("DNT", "1");
			urlConnection.setRequestProperty("Referer", urlStr);
			urlConnection.setRequestProperty("Accept",
					"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			urlConnection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.9) Gecko/20100101 Firefox/"
							+ String.format("%.2f", 10 + Math.random() * 60));
			if (nbRedirect < 16 && urlConnection.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP
					|| urlConnection.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM
					|| urlConnection.getResponseCode() == HttpURLConnection.HTTP_SEE_OTHER) {
				redirect = true;
				nbRedirect++;
				urlStr = urlConnection.getHeaderField("Location").trim();
			} else {
				redirect = false;
			}
		} while (redirect);
		return urlConnection;
	}

	@Override
	public void setCursor(Cursor cursor) {
		super.setCursor(cursor);
		scriptTP.setCursor(cursor);
		infoTP.setCursor(cursor);
		searchEdit.setCursor(cursor);
	}

	private void updateMenu() {
		getJMenuBar().getMenu(1).getItem(0).setEnabled(undoManager.canUndo());
		getJMenuBar().getMenu(1).getItem(1).setEnabled(undoManager.canRedo());
		main.updateMenu();
	}

	private void restartUndoRedo() {
		if (undoManager != null)
			scriptTP.getDocument().removeUndoableEditListener(undoManager);
		undoManager = new UndoManager() {
			private static final long serialVersionUID = 1L;

			@Override
			public synchronized boolean addEdit(UndoableEdit anEdit) {
				boolean res = true;
				res = super.addEdit(anEdit);
				syntaxHighlighter.fireChange(false);
				updateMenu();
				return res;
			}
		};
		scriptTP.getDocument().addUndoableEditListener(undoManager);
		updateMenu();
		syntaxHighlighter.fireChange(false);
	}

	private void undo() {
		undoManager.undo();
		updateMenu();
		syntaxHighlighter.fireChange(false);
	}

	private void redo() {
		undoManager.redo();
		updateMenu();
		syntaxHighlighter.fireChange(false);
	}

	private void search(KeyEvent e) {
		if (searchEdit.getText().length() == 0)
			searchEdit.setBackground(Color.WHITE);
		else if (scriptTP.getText().toLowerCase().contains(searchEdit.getText().toLowerCase())) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER)
				scriptTP.requestFocus();
			scriptTP.setSelectionStart(scriptTP.getText().toLowerCase().indexOf(searchEdit.getText().toLowerCase()));
			scriptTP.setSelectionEnd(scriptTP.getText().toLowerCase().indexOf(searchEdit.getText().toLowerCase())
					+ searchEdit.getText().length());
			searchEdit.setBackground(Color.WHITE);
		} else
			searchEdit.setBackground(Color.RED);
	}

	private void searchNext(boolean reverse) {
		String sStr = searchEdit.getText().toLowerCase();
		String text = scriptTP.getText().toLowerCase();
		if (sStr.trim().isEmpty()) {
			searchEdit.requestFocus();
		} else {
			scriptTP.requestFocus();
			if (text.contains(sStr)) {
				searchEdit.setBackground(Color.WHITE);
				int pos = scriptTP.getCaretPosition();
				if (reverse) {
					if (pos > 0 && text.substring(0, pos - 1).contains(sStr)) {
						scriptTP.setSelectionStart(text.substring(0, pos - 1).lastIndexOf(sStr));
						scriptTP.setSelectionEnd(text.substring(0, pos - 1).lastIndexOf(sStr) + sStr.length());
					} else {
						scriptTP.setSelectionStart(text.lastIndexOf(sStr));
						scriptTP.setSelectionEnd(text.lastIndexOf(sStr) + sStr.length());
					}

				} else if (pos < text.length() + 1 && text.substring(pos + 1).contains(sStr)) {
					scriptTP.setSelectionStart(pos + 1 + text.substring(pos + 1).indexOf(sStr));
					scriptTP.setSelectionEnd(pos + 1 + text.substring(pos + 1).indexOf(sStr) + sStr.length());
				} else {
					scriptTP.setSelectionStart(text.indexOf(sStr));
					scriptTP.setSelectionEnd(text.indexOf(sStr) + sStr.length());
				}
			} else {
				searchEdit.setBackground(Color.RED);
			}
		}
	}

	private void replaceAll() {
		String sStr = searchEdit.getText();
		if (sStr.trim().isEmpty()) {
			searchEdit.requestFocus();
		} else {
			String replaceBy = JOptionPane.showInputDialog(main.getString("editor.replace_with", sStr), sStr);
			if (replaceBy != null) {
				int n = 0;
				String s = scriptTP.getText();
				while (s.length() > 0 && s.contains(sStr)) {
					s = s.substring(s.indexOf(sStr) + sStr.length());
					n++;
				}
				scriptTP.setText(scriptTP.getText().replace(sStr, replaceBy));
				addInfo(main.getString("editor.replaced", n));
			}
		}
	}

	private void cutScript(ActionEvent e) {
		Clipboard clipboard = getToolkit().getSystemClipboard();
		StringSelection data = new StringSelection(scriptTP.getSelectedText());
		clipboard.setContents(data, data);
		try {
			scriptTP.getDocument().remove(scriptTP.getSelectionStart(),
					scriptTP.getSelectionEnd() - scriptTP.getSelectionStart());
		} catch (BadLocationException e1) {
			System.out.println(e);
		}
	}

	private void copyScript() {
		Clipboard clipboard = getToolkit().getSystemClipboard();
		StringSelection data = new StringSelection(scriptTP.getSelectedText());
		clipboard.setContents(data, data);
	}

	protected boolean scriptTPtabPressed(boolean shiftPressed) {
		int realEnd = scriptTP.getSelectionEnd();
		if (scriptTP.getSelectedText() == null) {
			if (shiftPressed) {
				realEnd = scriptTP.getCaretPosition();
			} else
				return false;
		}
		List<Integer> positions = new ArrayList<Integer>();
		boolean possible = true;
		try {
			int realStart = scriptTP.getSelectionStart();
			while (realStart > 0 && !scriptTP.getDocument().getText(realStart, 1).equals("\n"))
				realStart--;
			for (int n = realStart; n < realEnd; n++)
				if (n == 0 || scriptTP.getDocument().getText(n - 1, 1).equals("\n")) {
					positions.add(n);
					if (shiftPressed && !scriptTP.getDocument().getText(n, 1).equals("\t"))
						possible = false;
				}
			if (possible) {
				if (shiftPressed)
					for (int i = positions.size() - 1; i >= 0; i--)
						scriptTP.getDocument().remove(positions.get(i), 1);
				else
					for (int i = positions.size() - 1; i >= 0; i--)
						scriptTP.getDocument().insertString(positions.get(i), "\t", null);
			}
		} catch (BadLocationException e) {
		}
		return true;
	}

	private void useSnippet(Snippet sn) {
		String text = scriptTP.getText();
		int newCaretPos = scriptTP.getCaretPosition();
		String snippetText = sn.getSnippet();
		if (sn.getSnippet().contains("${cursor}"))
			newCaretPos += snippetText.indexOf("${cursor}");
		else
			newCaretPos += snippetText.length();
		snippetText = snippetText.replace("${cursor}", "");
		ScriptContainer sc = main.getScriptContainer();
		if (sc.currentScriptFileName != null)
			snippetText = snippetText.replace("${scriptname}", sc.currentScriptFileName);
		text = text.substring(0, scriptTP.getCaretPosition()) + snippetText
				+ text.substring(scriptTP.getCaretPosition());
		scriptTP.setText(text);
		scriptTP.setCaretPosition(newCaretPos);
		syntaxHighlighter.fireChange(false);
	}

	private void caretUpdate(CaretEvent ce) {
		int y = 0;
		int x = 0;
		char[] ct = scriptTP.getText().toCharArray();
		int n = 0;
		lastCaretDot = ce.getDot();
		for (char c : ct) {
			if (n == lastCaretDot)
				break;
			x++;
			n++;
			if (c == '\n') {
				y++;
				x = 0;
			}
		}
		infoLabel.setText((1 + y + INVISIBLE_LINES_COUNT) + " : " + (1 + x));
		// caretUpdateStyle();
		syntaxHighlighter.fireChange(true);
	}

	public String getClipboardUrl() {
		String res = "";
		Clipboard clipboard = getToolkit().getSystemClipboard();
		try {
			String content = (String) clipboard.getContents(this).getTransferData(DataFlavor.stringFlavor);
			String[] strings = content.split("\n");
			if (strings[0].contains("://"))
				res = strings[0];
		} catch (IOException e1) {
		} catch (UnsupportedFlavorException e1) {
		}
		return res;
	}

	private class SyntaxHighlighter implements Runnable {
		// bigger than mean lapse between two key hitted, but as small as
		// possible
		private static final int TIMER_MS = 200;
		private static final int SLOW_CODEASSIST_THRESHOLD_MS = 500;

		private int eventLevel = 0;
		private int timesToWait = 0;

		public void fireChange(boolean onlyCaret) {
			timesToWait = 1;
			if (onlyCaret)
				eventLevel = 1;
			else
				eventLevel = 2;
		}

		@Override
		public void run() {
			if (timesToWait > 0)
				timesToWait--;
			else {
				if (eventLevel >= 2 && (assistLevel & (8 + 16 + 32)) != 0)
					updateFromModification();
				if (eventLevel >= 1 && (assistLevel & 4) != 0)
					updateFromCaretMove();
				eventLevel = 0;
			}
			Timer t = new Timer(SyntaxHighlighter.TIMER_MS, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					SyntaxHighlighter.this.run();
				}
			});
			t.setRepeats(false);
			t.start();
		}

		public void updateFromModification() {
			long startTime = System.currentTimeMillis();
			// http://groovy-lang.org/syntax.html + ?
			final String[] keywords = { "abstract", "as", "assert", "boolean", "break", "byte", "case", "catch", "char",
					"class", "const", "continue", "def", "default", "do", "double", "else", "enum", "extends", "false",
					"final", "finally", "float", "for", "goto", "if", "implements", "import", "in", "instanceof", "int",
					"interface", "long", "native", "new", "null", "package", "private", "protected", "public", "return",
					"short", "static", "strictfp", "super", "switch", "synchronized", "this", "threadsafe", "throw",
					"throws", "trait", "transient", "true", "try", "void", "volatile", "while" };
			List<String> methodNames = new ArrayList<String>();
			for (Method m : Script.class.getMethods())
				methodNames.add(m.getName());
			StyledDocument sd = scriptTP.getStyledDocument();
			SimpleAttributeSet nullAS = new SimpleAttributeSet();
			SimpleAttributeSet keywordsAS = new SimpleAttributeSet();
			StyleConstants.setBold(keywordsAS, true);
			StyleConstants.setForeground(keywordsAS, new Color(256 * 256 * 250 + 100));
			SimpleAttributeSet methodsAS = new SimpleAttributeSet();
			StyleConstants.setBold(methodsAS, true);
			StyleConstants.setForeground(methodsAS, new Color(256 * 256 * 100 + 250));
			SimpleAttributeSet stringAS = new SimpleAttributeSet();
			StyleConstants.setForeground(stringAS, new Color(257 * 120));
			SimpleAttributeSet commentsAS = new SimpleAttributeSet();
			StyleConstants.setForeground(commentsAS, new Color(256 * 140));
			SimpleAttributeSet searchAS = new SimpleAttributeSet();
			StyleConstants.setBackground(searchAS, new Color(256 * 257 * 200 + 255));

			String text = scriptTP.getText();
			scriptTP.getDocument().removeUndoableEditListener(undoManager);
			sd.setCharacterAttributes(0, sd.getLength(), nullAS, true);
			// keywords and methods
			if ((assistLevel & 8) != 0) {
				int n = 0;
				String[] words = text.split("[^a-zA-Z0-9]");
				for (String word : words) {
					int l = word.length();
					if (l > 1) {
						for (String keyword : keywords)
							if (word.equals(keyword))
								sd.setCharacterAttributes(n, l, keywordsAS, false);
					}
					if (l > 2) {
						for (String methodName : methodNames)
							if (word.equals(methodName))
								sd.setCharacterAttributes(n, l, methodsAS, false);
					}
					n += l + 1;
				}
				/*
				 * this alternative is slower for (String keyword : keywords) {
				 * int l = keyword.length(); int n = 0; int i = 0; do { i =
				 * text.indexOf(keyword, n); if(i>-1) {
				 * sd.setCharacterAttributes(i+n, l, keywordsAS, false); n =
				 * i+l; } } while(i!=-1); } for (String methodName :
				 * methodNames) { int l = methodName.length(); int n = 0; int i
				 * = 0; do { i = text.indexOf(methodName, n); if(i>-1) {
				 * sd.setCharacterAttributes(i+n, l, methodsAS, false); n = i+l;
				 * } } while(i!=-1); }
				 */
			}
			// strings, comments
			if ((assistLevel & 16) != 0) {
				String[] lines = text.split("\"");
				int n = 0;
				boolean inString = false;
				for (String line : lines) {
					if (inString)
						sd.setCharacterAttributes(n, line.length(), stringAS, false);
					n += line.length() + 1;
					if (!line.endsWith("\\"))
						inString = !inString;
				}
				lines = text.split("\n");
				n = 0;
				for (String line : lines) {
					if (line.indexOf("//") > -1
							&& (line.indexOf("//") == 0 || line.charAt(line.indexOf("//") - 1) != ':'))
						sd.setCharacterAttributes(n + line.indexOf("//"), line.length() - line.indexOf("//"),
								commentsAS, true);
					n += line.length() + 1;
				}
				lines = text.split("\\*" + "/");
				n = 0;
				for (String line : lines) {
					if (line.indexOf("/*") > -1)
						sd.setCharacterAttributes(n + line.indexOf("/*"), line.length() - line.indexOf("/*"),
								commentsAS, true);
					n += line.length() + 1;
				}
			}
			// search
			if ((assistLevel & 32) != 0) {
				SimpleAttributeSet noPairAS = new SimpleAttributeSet();
				StyleConstants.setBackground(noPairAS, new Color(256 * 257 * 255 + 255));
				sd.setCharacterAttributes(0, scriptTP.getText().length(), noPairAS, false);
				String sText = searchEdit.getText().trim().toLowerCase();
				String lText = text.toLowerCase();
				if (sText.length() > 1) {
					int n = 0;
					while (lText.substring(n).contains(sText)) {
						n += lText.substring(n).indexOf(sText);
						sd.setCharacterAttributes(n, sText.length(), searchAS, false);
						n += sText.length();
					}
				}
			}
			scriptTP.getDocument().addUndoableEditListener(undoManager);
			long endTime = System.currentTimeMillis();
			long time = endTime - startTime;
			boolean tooSlow = time > SLOW_CODEASSIST_THRESHOLD_MS;
			if (tooSlow) {
				if ((assistLevel & 8) != 0) {
					assistLevel = assistLevel & ~8;
					addInfo("Code assist too slow (" + time + "ms > " + SLOW_CODEASSIST_THRESHOLD_MS
							+ "ms), application.editor.assistlevel=" + assistLevel
							+ " applied (but not saved to data.properties)");
				} else if ((assistLevel & 16) != 0) {
					assistLevel = assistLevel & ~16;
					addInfo("Code assist too slow (" + time + "ms > " + SLOW_CODEASSIST_THRESHOLD_MS
							+ "ms), application.editor.assistlevel=" + assistLevel
							+ " applied (but not saved to data.properties)");
				}
			}
		}

		private void updateFromCaretMove() {
			final String[] cars1 = { "(", "[", "{", ")", "]", "}" };
			final String[] cars2 = { ")", "]", "}", "(", "[", "{" };
			String car;
			try {
				int currentPos = scriptTP.getCaretPosition();
				car = scriptTP.getText(currentPos, 1);
				if (!Arrays.asList(cars1).contains(car) && currentPos > 0) {
					currentPos--;
					car = scriptTP.getText(currentPos, 1);
				}
				if (Arrays.asList(cars1).contains(car)) {
					boolean before = Arrays.asList(cars1).indexOf(car) >= cars1.length / 2;
					String otherCar = cars2[Arrays.asList(cars1).indexOf(car)];
					int foundPos = 0;
					int inclusionLevel = 0;
					String fulltext = scriptTP.getText();
					int p = currentPos;
					if (before) {
						String text = fulltext.substring(0, p);
						while (foundPos == 0 && p > 0) {
							if (text.lastIndexOf(otherCar) == -1)
								break;
							if (text.lastIndexOf(otherCar) > text.lastIndexOf(car)) {
								if (inclusionLevel > 0) {
									inclusionLevel--;
									p = text.lastIndexOf(otherCar);
								} else {
									foundPos = text.lastIndexOf(otherCar);
									break;
								}
							} else if (text.lastIndexOf(otherCar) < text.lastIndexOf(car)) {
								inclusionLevel++;
								p = text.lastIndexOf(car);
							}
							text = text.substring(0, p);
						}
					} else {
						String text = fulltext.substring(p + 1);
						int lostCars = p + 1;
						while (foundPos == 0 && text.length() > 0) {
							int pOtherCar = text.indexOf(otherCar);
							if (pOtherCar == -1)
								break;
							if ((text.indexOf(car) == -1 && pOtherCar > -1) || pOtherCar < text.indexOf(car)) {
								if (inclusionLevel > 0) {
									inclusionLevel--;
									p = text.indexOf(otherCar);
								} else {
									foundPos = pOtherCar + lostCars;
									break;
								}
							} else if (text.indexOf(otherCar) > text.indexOf(car)) {
								inclusionLevel++;
								p = text.indexOf(car);
							}
							text = text.substring(p + 1);
							lostCars += p + 1;
						}
					}
					scriptTP.getDocument().removeUndoableEditListener(undoManager);
					StyledDocument sd = scriptTP.getStyledDocument();
					SimpleAttributeSet pairAS = new SimpleAttributeSet();
					if (foundPos > 0)
						// Yellow
						StyleConstants.setBackground(pairAS, new Color(256 * 257 * 255 + 120));
					else
						// Red
						StyleConstants.setBackground(pairAS, new Color(256 * 256 * 255 + 120 * 257));
					sd.setCharacterAttributes(currentPos, 1, pairAS, false);
					if (foundPos > 0)
						sd.setCharacterAttributes(foundPos, 1, pairAS, false);
					scriptTP.getDocument().addUndoableEditListener(undoManager);
				}
			} catch (BadLocationException e) {
				e.printStackTrace();
			}

		}
	}

	private class LinkPosition {
		public int start;
		public int end;
		public int line;

		public LinkPosition(int start, int end, int line) {
			this.start = start;
			this.end = end;
			this.line = line;
		}
	}
}
