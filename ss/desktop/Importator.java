/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import ss.TextSource;

/**
 * Implements something to import to....groovy
 * 
 * @author Doti
 */
public abstract class Importator {

	public final static int PICTURE_MAX_WIDTH = 900;
	public final static int PICTURE_MAX_HEIGHT = 800;

	public abstract String doImport(TextSource textSource, EditorFrame editor);

	public void resizeIfNeeded(File file) throws IOException {
		BufferedImage bi = javax.imageio.ImageIO.read(file);
		int width = bi.getWidth();
		int height = bi.getHeight();
		boolean resize = false;
		if (width > PICTURE_MAX_WIDTH) {
			height = (int) (height * PICTURE_MAX_WIDTH / width);
			width = PICTURE_MAX_WIDTH;
			resize = true;
		}
		if (height > PICTURE_MAX_HEIGHT) {
			width = (int) (width * PICTURE_MAX_WIDTH / height);
			height = PICTURE_MAX_WIDTH;
			resize = true;
		}
		if (resize) {
			Image rIm = bi.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			BufferedImage rBi = new BufferedImage(width, height, bi.getType());
			rBi.createGraphics().drawImage(rIm, 0, 0, null);
			File newFile = file;
			if (!file.getName().toLowerCase().endsWith(".jpeg")
					&& !file.getName().toLowerCase().endsWith(".jpg")) {
				String nameWOext = file.getAbsolutePath();
				nameWOext = nameWOext.substring(0, nameWOext.lastIndexOf("."));
				newFile = new File(nameWOext + ".jpeg");
			}
			ImageIO.write(rBi, "JPEG", newFile);
		}
	}
}