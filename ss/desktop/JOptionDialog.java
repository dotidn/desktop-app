/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
  */
package ss.desktop;

import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

/**
 * Show some options (for toys, etc.)
 * @author Doti
 */
public class JOptionDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;

	private List<JCheckBox> checkboxes;
	private JButton okBtn;
	private List<Boolean> modalResult;

	public JOptionDialog(MainFrame main, String title, String text,
			List<String> options, List<Boolean> defaultValues)
			throws HeadlessException {
		super(main, title, true);
		setResizable(false);
				
		checkboxes = new ArrayList<JCheckBox>();

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBackground(main.getBackground());
		panel.setBorder(new EmptyBorder(5,5,5,5));
		setContentPane(panel);
		
		JLabel labelText = new JLabel(text,SwingConstants.LEFT);
		labelText.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(labelText);
		
		JPanel panelCenter = new JPanel();
		panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.X_AXIS));
		panelCenter.setBackground(main.getBackground());
		panel.add(panelCenter);
		
		JPanel panel1 = new JPanel();
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		panel1.setBackground(main.getBackground());
		panelCenter.add(panel1);

		JPanel panel2 = new JPanel();
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
		panel2.setBackground(main.getBackground());
		panelCenter.add(panel2);		

		int n = 0;
		for (String option : options) {
			JCheckBox cb = new JCheckBox(option);
			cb.setBackground(main.getBackground());
			if(defaultValues!=null && defaultValues.size()>n && defaultValues.get(n))
				cb.setSelected(true);
			if(n<options.size()/2)
				panel1.add(cb);
			else
				panel2.add(cb);
			checkboxes.add(cb);
			n++;
		}

		JPanel bottomPanel = new JPanel();
		bottomPanel.setBackground(main.getBackground());
		panel.add(bottomPanel);
		
		okBtn = new JButton(UIManager.getString("OptionPane.okButtonText"));
		okBtn.addActionListener(this);
		bottomPanel.add(okBtn);

		JButton b2 = new JButton(UIManager
				.getString("OptionPane.cancelButtonText"));
		b2.addActionListener(this);
		bottomPanel.add(b2);

		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == okBtn) {
			modalResult = new ArrayList<Boolean>();
			for (JCheckBox cb : checkboxes)
				modalResult.add(cb.isSelected());
		}
		dispose();
	}

	public List<Boolean> getModalResult() {
		return modalResult;
	}
}
